FROM python

SHELL ["/bin/bash", "-c"]

RUN mkdir /app
COPY /app /app
COPY pyproject.toml /app

WORKDIR /app
ENV PYTHONPATH=${PYTHONPATH}:${PWD}

RUN pip3 install poetry
COPY poetry.lock pyproject.toml .
RUN poetry config virtualenvs.create false
RUN poetry install --no-dev --no-interaction --no-cache

COPY . /app

EXPOSE 5000
ENTRYPOINT ["poetry","run","python","-m","flask","run","--host=0.0.0.0"]